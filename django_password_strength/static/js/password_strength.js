$(document).ready(function() {
	$('.password_strength').keyup(function() {
        var bar = $('#password_strength_bar');
        var info = $('#password_strength_info');

		if( $(this).val() ) {
			result = zxcvbn( $(this).val() );

			if( result.score < 3 ) {
				bar.removeClass('bar-success').addClass('bar-warning');
				info.html('<span class="label label-important">Warning</span> This password would take <em>'+result.crack_time_display+'</em> to crack.');
			} else {
				bar.removeClass('bar-warning').addClass('bar-success');
				info.html('This password would take <em>'+result.crack_time_display+'</em> to crack.');
			}

			bar.width( ((result.score+1)/5)*100 + '%' );

		} else {
			bar.removeClass('bar-success').addClass('bar-warning');
			bar.width('0%');
			bar.attr('aria-valuenow', 0);
			info.html('');
		}
	});
});
