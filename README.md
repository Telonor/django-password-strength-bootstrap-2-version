# Django Password Strength (Boostrap 2 version)

## A Smarter Password Strength Meter

An extension of the Django password widget including a password strength meter and crack time powered by [zxcvbn](https://github.com/lowe/zxcvbn).

### Usage:

* Add to your Django Project
* Instead of using the django `PasswordInput` widget use the `PasswordStrengthInput`
* Be sure to include the form's required media in the template. _ie._ `{{ form.media }}`
* For easiest integration also include [Twitter Bootstrap](http://getbootstrap.com/2.3.2/)

### Example:

_forms.py_

	from django import forms
	from django_password_strength.widgets import PasswordStrengthInput
    
    class SignupForm(forms.Form):
    	username = forms.CharField()
    	passphrase = forms.CharField(
        	widget=PasswordStrengthInput()
        )